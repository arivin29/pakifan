/**
 * INSPINIA - Responsive Admin Theme
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
    $urlRouterProvider.otherwise("/index/main");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "views/common/content.html",
        })
        .state('index.main', {
            url: "/main",
            templateUrl: "views/main.html",
            data: { pageTitle: 'Dashboard' }
        })
        .state('index.employee-dashboard', {
            url: "/employee-dashboard",
            templateUrl: "views/app/employee-dashboard.html",
            data: { pageTitle: 'Employee Dashboard' }
        })

        /*--- Company ---*/
        .state('index.company-settings', {
            url: "/company-settings",
            templateUrl: "views/app/company-settings.html",
            data: { pageTitle: 'Company Settings' }
        })
        .state('index.site', {
            url: "/site",
            templateUrl: "views/app/site.html",
            data: { pageTitle: 'Site view' }
        })
        .state('index.departments', {
            url: "/departments",
            templateUrl: "views/app/departments.html",
            data: { pageTitle: 'Departments view' }
        })
        .state('index.departments1', {
            url: "/departments1",
            templateUrl: "views/app/departments1.html",
            data: { pageTitle: 'Departments1 view' }
        })
        .state('index.departments.popup', {
            url: "/popup",
            templateUrl: "views/app/departments/add.html",
            data: { pageTitle: 'Departments view' }
        })
        .state('index.divisions', {
            url: "/divisions",
            templateUrl: "views/app/divisions.html",
            data: { pageTitle: 'Divisions' }
        })
        .state('index.positions', {
            url: "/positions",
            templateUrl: "views/app/positions.html",
            data: { pageTitle: 'Positions view' }
        })
        .state('index.positions-chart', {
            url: "/positions-chart",
            templateUrl: "views/app/positions-chart.html",
            data: { pageTitle: 'Positions view' }
        })
        .state('index.jobgrade', {
            url: "/jobgrade",
            templateUrl: "views/app/jobgrade.html",
            data: { pageTitle: 'Job Grade view' }
        })
        .state('index.events', {
            url: "/events",
            templateUrl: "views/app/events.html",
            data: { pageTitle: 'Events' }
        })
        .state('index.announcement', {
            url: "/announcement",
            templateUrl: "views/app/announcement.html",
            data: { pageTitle: 'Announcement' }
        })
        .state('index.legal', {
            url: "/legal",
            templateUrl: "views/app/legal.html",
            data: { pageTitle: 'Legal' }
        })
        .state('index.certificates', {
            url: "/certificates",
            templateUrl: "views/app/certificates.html",
            data: { pageTitle: 'certificates' }
        })

        /*--- Employees ---*/
        .state('index.employees', {
            url: "/employees",
            templateUrl: "views/app/employees.html",
            data: { pageTitle: 'Employees' }
        })
        .state('index.emp-projects', {
            url: "/emp-projects",
            templateUrl: "views/app/emp-projects.html",
            data: { pageTitle: 'Employees' }
        })
        .state('index.emp-bank', {
            url: "/emp-bank",
            templateUrl: "views/app/emp-bank.html",
            data: { pageTitle: 'Employees' }
        })
        .state('index.emp-licenses', {
            url: "/emp-licenses",
            templateUrl: "views/app/emp-licenses.html",
            data: { pageTitle: 'Employees' }
        })
        .state('index.employees-list', {
            url: "/employees-list",
            templateUrl: "views/app/employees-list.html",
            data: { pageTitle: 'Employees List' }
        })
        .state('index.profile', {
            url: "/profile",
            templateUrl: "views/app/profile.html",
            data: { pageTitle: 'Profile' }
        })
        .state('index.licenses', {
            url: "/licenses",
            templateUrl: "views/app/licenses.html",
            data: { pageTitle: 'Licenses' }
        })
        .state('index.licenses-employee', {
            url: "/licenses-employee",
            templateUrl: "views/app/licenses-employee.html",
            data: { pageTitle: 'Licenses Employee' }
        })
        .state('index.training', {
            url: "/training",
            templateUrl: "views/app/training.html",
            data: { pageTitle: 'Training' }
        })
        .state('index.training-list', {
            url: "/training-list",
            templateUrl: "views/app/training-list.html",
            data: { pageTitle: 'Training List' }
        })
        .state('index.training-view', {
            url: "/training-view",
            templateUrl: "views/app/training-view.html",
            data: { pageTitle: 'Training View' }
        })
        .state('index.projects', {
            url: "/projects",
            templateUrl: "views/app/projects.html",
            data: { pageTitle: 'Projects' }
        })
        .state('index.projects-list', {
            url: "/projects-list",
            templateUrl: "views/app/projects-list.html",
            data: { pageTitle: 'Projects List' }
        })
        .state('index.projects-view', {
            url: "/projects-view",
            templateUrl: "views/app/projects-view.html",
            data: { pageTitle: 'Projects View' }
        })
        .state('index.performance', {
            url: "/performance",
            templateUrl: "views/app/performance.html",
            data: { pageTitle: 'Performance' }
        })
        .state('index.warningletter', {
            url: "/warningletter",
            templateUrl: "views/app/warningletter.html",
            data: { pageTitle: 'Warning Letter' }
        })
        .state('index.promotion', {
            url: "/promotion",
            templateUrl: "views/app/promotion.html",
            data: { pageTitle: 'Promotion' }
        })
        .state('index.resignation', {
            url: "/resignation",
            templateUrl: "views/app/resignation.html",
            data: { pageTitle: 'Resignation' }
        })
        .state('index.termination', {
            url: "/termination",
            templateUrl: "views/app/termination.html",
            data: { pageTitle: 'Termination' }
        })
        .state('index.jobs', {
            url: "/jobs",
            templateUrl: "views/app/jobs.html",
            data: { pageTitle: 'Jobs' }
        })
        .state('index.job-applicants', {
            url: "/jobs-applicants",
            templateUrl: "views/app/job-applicants.html",
            data: { pageTitle: 'Job Applicants' }
        })
        .state('index.job-details', {
            url: "/job-details",
            templateUrl: "views/app/job-details.html",
            data: { pageTitle: 'Job Details' }
        })

        /*--- Schedule ---*/
        .state('index.attendance', {
            url: "/attendance",
            templateUrl: "views/app/attendance.html",
            data: { pageTitle: 'Attendance' }
        })
        .state('index.attendance-employee', {
            url: "/attendance-employee",
            templateUrl: "views/app/attendance-employee.html",
            data: { pageTitle: 'Attendance Employee' }
        })
        .state('index.attendance-view', {
            url: "/attendance-view",
            templateUrl: "views/app/attendance-view.html",
            data: { pageTitle: 'Attendance View' }
        })
        .state('index.attendance-edit', {
            url: "/attendance-edit",
            templateUrl: "views/app/attendance-edit.html",
            data: { pageTitle: 'Attendance Edit' }
        })
        .state('index.attendance.view', {
            url: "/view",
            templateUrl: "views/app/attendance/view.html",
            data: { pageTitle: 'Attendance View' }
        })
        .state('index.attendance.edit', {
            url: "/edit",
            templateUrl: "views/app/attendance/edit.html",
            data: { pageTitle: 'Attendance Edit' }
        })
        .state('index.leaves', {
            url: "/leaves",
            templateUrl: "views/app/leaves.html",
            data: { pageTitle: 'Leaves' }
        })
        .state('index.leaves-employee', {
            url: "/leaves-employee",
            templateUrl: "views/app/leaves-employee.html",
            data: { pageTitle: 'Leaves Employee' }
        })
        .state('index.overtime', {
            url: "/overtime",
            templateUrl: "views/app/overtime.html",
            data: { pageTitle: 'Overtime' }
        })
        .state('index.onduty', {
            url: "/onduty",
            templateUrl: "views/app/onduty.html",
            data: { pageTitle: 'On Duty ' }
        })
        .state('index.holidays', {
            url: "/holidays",
            templateUrl: "views/app/holidays.html",
            data: { pageTitle: 'Holidays' }
        })
        .state('index.work-schedule', {
            url: "/work-schedule",
            templateUrl: "views/app/work-schedule.html",
            data: { pageTitle: 'Work Schedule' }
        })
        .state('index.schedule-setting', {
            url: "/schedule-setting",
            templateUrl: "views/app/schedule-setting.html",
            data: { pageTitle: 'Schedule Setting' }
        })

        /*--- Payroll ---*/
        .state('index.payroll-process', {
            url: "/payroll-process",
            templateUrl: "views/app/payroll-process.html",
            data: { pageTitle: 'Payroll Process' }
        })
        .state('index.payslip', {
            url: "/payslip",
            templateUrl: "views/app/payslip.html",
            data: { pageTitle: 'Payslip List' }
        })
        .state('index.payslip-employee-list', {
            url: "/payslip-employee-list",
            templateUrl: "views/app/payslip-employee-list.html",
            data: { pageTitle: 'Payslip Employee List' }
        })
        .state('index.payslip-print', {
            url: "/payslip-print",
            templateUrl: "views/app/payslip-print.html",
            data: { pageTitle: 'Payslip Print' }
        })
        .state('index.payment-print', {
            url: "/payment-print",
            templateUrl: "views/app/payment-print.html",
            data: { pageTitle: 'Payment Print' }
        })
        .state('index.bonus', {
            url: "/bonus",
            templateUrl: "views/app/bonus.html",
            data: { pageTitle: 'Commission / Bonus' }
        })
        .state('index.bonus-employee-list', {
            url: "/bonus-employee-list",
            templateUrl: "views/app/bonus-employee-list.html",
            data: { pageTitle: 'Bonus Employee List' }
        })
        .state('index.tax-setting', {
            url: "/tax-setting",
            templateUrl: "views/app/tax-setting.html",
            data: { pageTitle: 'Tax Setting' }
        })
        .state('index.basic-salary', {
            url: "/basic-salary",
            templateUrl: "views/app/basic-salary.html",
            data: { pageTitle: 'Basic Salary Range' }
        })
        .state('index.basic-salary-employee', {
            url: "/basic-salary-employee",
            templateUrl: "views/app/basic-salary-employee.html",
            data: { pageTitle: 'Basic Salary Employee' }
        })
        .state('index.basic-salary-psn', {
            url: "/basic-salary-psn",
            templateUrl: "views/app/basic-salary-psn.html",
            data: { pageTitle: 'Basic Salary PSN' }
        })
        .state('index.psn', {
            url: "/psn",
            templateUrl: "views/app/psn.html",
            data: { pageTitle: 'Basic Salary PSN' }
        })
        .state('index.payment-setting', {
            url: "/payment-setting",
            templateUrl: "views/app/payment-setting.html",
            data: { pageTitle: 'Payment Setting' }
        })
        .state('index.payroll-component', {
            url: "/payroll-component",
            templateUrl: "views/app/payroll-component.html",
            data: { pageTitle: 'Payroll Component' }
        })
        .state('index.payroll-component-employee', {
            url: "/payroll-component-employee",
            templateUrl: "views/app/payroll-component-employee.html",
            data: { pageTitle: 'Payroll Component Employee' }
        })
        .state('index.payroll-component-employee-add', {
            url: "/payroll-component-employee-add",
            templateUrl: "views/app/payroll-component-employee-add.html",
            data: { pageTitle: 'Add Payroll Component Employee Add' }
        })
        .state('index.payroll-component-employee-edit', {
            url: "/payroll-component-employee-edit",
            templateUrl: "views/app/payroll-component-employee-edit.html",
            data: { pageTitle: 'Add Payroll Component Employee Edit' }
        })
        .state('index.wage-limit', {
            url: "/wage-limit",
            templateUrl: "views/app/wage-limit.html",
            data: { pageTitle: 'Wage Limit' }
        })
        .state('index.contribution-percentage', {
            url: "/contribution-percentage",
            templateUrl: "views/app/contribution-percentage.html",
            data: { pageTitle: 'Contribution Percentage' }
        })
        .state('index.holiday-allow-process', {
            url: "/holiday-allow-process",
            templateUrl: "views/app/holiday-allow-process.html",
            data: { pageTitle: 'Holiday Allowance Process' }
        })
        .state('index.holiday-allow-component', {
            url: "/holiday-allow-component",
            templateUrl: "views/app/holiday-allow-component.html",
            data: { pageTitle: 'Holiday Allowance Component' }
        })
        .state('index.holiday-allow-setting', {
            url: "/holiday-allow-setting",
            templateUrl: "views/app/holiday-allow-setting.html",
            data: { pageTitle: 'Holiday Allowance Setting' }
        })
        .state('index.payroll-component-employee.add', {
            url: "/payroll-component-employee.add",
            templateUrl: "views/app/payroll-component-employee/add.html",
            data: { pageTitle: 'Add Payroll Component Employee' }
        })

        /*--- Contacts ---*/
        .state('index.contacts', {
            url: "/contacts",
            templateUrl: "views/app/contacts.html",
            data: { pageTitle: 'Contacts' }
        })
        .state('index.vendors', {
            url: "/vendors",
            templateUrl: "views/app/vendors.html",
            data: { pageTitle: 'Vendors' }
        })

        /*--- Assets ---*/
        .state('index.ga-assets', {
            url: "/ga-assets",
            templateUrl: "views/app/ga-assets.html",
            data: { pageTitle: 'GA Assets' }
        })
        .state('index.ga-cars', {
            url: "/ga-cars",
            templateUrl: "views/app/ga-cars.html",
            data: { pageTitle: 'Cars' }
        })
        .state('index.ga-cars-service', {
            url: "/ga-cars-service",
            templateUrl: "views/app/ga-cars-service.html",
            data: { pageTitle: 'Cars Service' }
        })
        .state('index.ga-consumables', {
            url: "/ga-consumables",
            templateUrl: "views/app/ga-consumables.html",
            data: { pageTitle: 'Consumables' }
        })
        .state('index.it-assets', {
            url: "/it-assets",
            templateUrl: "views/app/it-assets.html",
            data: { pageTitle: 'IT Assets' }
        })
        .state('index.it-assets-emp', {
            url: "/it-assets-emp",
            templateUrl: "views/app/it-assets-emp.html",
            data: { pageTitle: 'IT Assets Employees' }
        })
        .state('index.it-assets-main', {
            url: "/it-assets-main",
            templateUrl: "views/app/it-assets-main.html",
            data: { pageTitle: 'IT Assets Maintenance' }
        })
        .state('index.it-assets-view', {
            url: "/it-assets-view",
            templateUrl: "views/app/it-assets-view.html",
            data: { pageTitle: 'IT Assets View' }
        })
        .state('index.it-assets-components', {
            url: "/it-assets-components",
            templateUrl: "views/app/it-assets-components.html",
            data: { pageTitle: 'IT Assets Components' }
        })
        .state('index.it-assets-main-view', {
            url: "/it-assets-main-view",
            templateUrl: "views/app/it-assets-main-view.html",
            data: { pageTitle: 'IT Assets Maintenance' }
        })
        .state('index.it-components', {
            url: "/it-components",
            templateUrl: "views/app/it-components.html",
            data: { pageTitle: 'IT Components' }
        })

        /*--- Accounting ---*/
        .state('index.accounting', {
            url: "/accounting",
            templateUrl: "views/app/accounting.html",
            data: { pageTitle: 'Accounting' }
        })
        .state('index.accounting-potongan', {
            url: "/accounting-potongan",
            templateUrl: "views/app/accounting-potongan.html",
            data: { pageTitle: 'Accounting Potongan' }
        })

        /*--- Koperasi ---*/
        .state('index.koperasi', {
            url: "/koperasi",
            templateUrl: "views/app/koperasi.html",
            data: { pageTitle: 'Koperasi' }
        })
        .state('index.koperasi-members', {
            url: "/koperasi-members",
            templateUrl: "views/app/koperasi-members.html",
            data: { pageTitle: 'Koperasi Members' }
        })
        .state('index.koperasi-sp', {
            url: "/koperasi-sp",
            templateUrl: "views/app/koperasi-sp.html",
            data: { pageTitle: 'Simpanan / Pinjaman' }
        })

        .state('index.budget', {
            url: "/budget",
            templateUrl: "views/app/budget.html",
            data: { pageTitle: 'Budget' }
        })
        .state('index.budget-view', {
            url: "/budget",
            templateUrl: "views/app/budget-view.html",
            data: { pageTitle: 'Budget' }
        })
    // Setting------------------------------------------------------------------------------------
        .state('setting', {
            abstract: true,
            url: "/setting",
            templateUrl: "views/common/content-setting.html",
        })
        .state('setting.user', {
            url: "/user",
            templateUrl: "views/setting/user/list.html",
            data: { pageTitle: 'User' }
        })

        // Setting------------------------------------------------------------------------------------
        .state('filemanajemen', {
            abstract: true,
            url: "/filemanajemen",
            templateUrl: "views/common/content-filemanajemen.html",
        })
        .state('filemanajemen.user', {
            url: "/user",
            templateUrl: "views/filemanajemen/user/list.html",
            data: { pageTitle: 'User' }
        })

}
angular
    .module('inspinia')
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });
