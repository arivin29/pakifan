/**
 * INSPINIA - Responsive Admin Theme
 *
 */


/**
 * pageTitle - Directive for set Page title - mata title
 */
function pageTitle($rootScope, $timeout) {
    return {
        link: function (scope, element) {
            var listener = function (event, toState, toParams, fromState, fromParams) {
                // Default title - load on Dashboard 1
                var title = 'GSMS | Responsive Admin Theme';
                // Create your own title pattern
                if (toState.data && toState.data.pageTitle) title = 'GSMS | ' + toState.data.pageTitle;
                $timeout(function () {
                    element.text(title);
                });
            };
            $rootScope.$on('$stateChangeStart', listener);
        }
    }
}

/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element) {

            var $wrapper = $('.main-wrapper');
            var $pageWrapper = $('.page-wrapper');
            var $slimScrolls = $('.slimscroll');
            var $sidebarOverlay = $('.sidebar-overlay');

            // Sidebar

            var Sidemenu = function () {
                this.$menuItem = $('#sidebar-menu a');
            };


            var $this = Sidemenu;
            $('#sidebar-menu a').on('click', function (e) {
                if ($(this).parent().hasClass('submenu')) {
                    e.preventDefault();
                }
                if (!$(this).hasClass('subdrop')) {
                    $('ul', $(this).parents('ul:first')).slideUp(350);
                    $('a', $(this).parents('ul:first')).removeClass('subdrop');
                    $(this).next('ul').slideDown(350);
                    $(this).addClass('subdrop');
                    $(".submenu .active").parent().parent().css('display', 'block');
                } else if ($(this).hasClass('subdrop')) {
                    $(this).removeClass('subdrop');
                    $(this).next('ul').slideUp(350);
                }
            });
            $('#sidebar-menu ul li.submenu a.active').parents('li:last').children('a:first').addClass('active').trigger('click');


            // Sidebar overlay

            function sidebar_overlay($target) {
                if ($target.length) {
                    $target.toggleClass('opened');
                    $sidebarOverlay.toggleClass('opened');
                    $('html').toggleClass('menu-opened');
                    $sidebarOverlay.attr('data-reff', '#' + $target[0].id);
                }
            }

            // Mobile menu sidebar overlay

            $(document).on('click', '#mobile_btn', function () {
                var $target = $($(this).attr('href'));
                sidebar_overlay($target);
                $wrapper.toggleClass('slide-nav');
                $('#task_window').removeClass('opened');
                return false;
            });

            // Chat sidebar overlay

            $(document).on('click', '#task_chat', function () {
                var $target = $($(this).attr('href'));
                sidebar_overlay($target);
                return false;
            });

            // Sidebar overlay reset

            $sidebarOverlay.on('click', function () {
                var $target = $($(this).attr('data-reff'));
                if ($target.length) {
                    $target.removeClass('opened');
                    $('html').removeClass('menu-opened');
                    $(this).removeClass('opened');
                    $wrapper.removeClass('slide-nav');
                }
                return false;
            });

            // Select 2

            if ($('.select').length > 0) {
                $('.select').select2({
                    minimumResultsForSearch: -1,
                    width: '100%'
                });
            }

            // Modal Popup hide show

            if ($('.modal').length > 0) {
                var modalUniqueClass = ".modal";
                $('.modal').on('show.bs.modal', function (e) {
                    var $element = $(this);
                    var $uniques = $(modalUniqueClass + ':visible').not($(this));
                    if ($uniques.length) {
                        $uniques.modal('hide');
                        $uniques.one('hidden.bs.modal', function (e) {
                            $element.modal('show');
                        });
                        return false;
                    }
                });
            }

            // Floating Label

            if ($('.floating').length > 0) {
                $('.floating').on('focus blur', function (e) {
                    $(this).parents('.form-focus').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
                }).trigger('blur');
            }

            // Sidebar Slimscroll

            if ($slimScrolls.length > 0) {
                $slimScrolls.slimScroll({
                    height: 'auto',
                    width: '100%',
                    position: 'right',
                    size: '7px',
                    color: '#ccc',
                    wheelStep: 10,
                    touchScrollStep: 100
                });
                var wHeight = $(window).height() - 60;
                $slimScrolls.height(wHeight);
                $('.sidebar .slimScrollDiv').height(wHeight);
                $(window).resize(function () {
                    var rHeight = $(window).height() - 60;
                    $slimScrolls.height(rHeight);
                    $('.sidebar .slimScrollDiv').height(rHeight);
                });
            }

            // Page Content Height

            var pHeight = $(window).height();
            $pageWrapper.css('min-height', pHeight);
            $(window).resize(function () {
                var prHeight = $(window).height();
                $pageWrapper.css('min-height', prHeight);
            });

            $(document).on('click', '#toggle_btn', function () {
                if ($('body').hasClass('mini-sidebar')) {
                    $('body').removeClass('mini-sidebar');
                    $('.subdrop + ul').slideDown();
                } else {
                    $('body').addClass('mini-sidebar');
                    $('.subdrop + ul').slideUp();
                }
                return false;
            });
            $(document).on('mouseover', function (e) {
                e.stopPropagation();
                if ($('body').hasClass('mini-sidebar') && $('#toggle_btn').is(':visible')) {
                    var targ = $(e.target).closest('.sidebar').length;
                    if (targ) {
                        $('body').addClass('expand-menu');
                        $('.subdrop + ul').slideDown();
                    } else {
                        $('body').removeClass('expand-menu');
                        $('.subdrop + ul').slideUp();
                    }
                    return false;
                }
            });


        }
    };
}

/**
 * iboxTools - Directive for iBox tools elements in right corner of ibox
 */
function iboxTools($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.children('.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            },
                // Function for close ibox
                $scope.closebox = function () {
                    var ibox = $element.closest('div.ibox');
                    ibox.remove();
                }
        }
    };
}

/**
 * iboxTools with full screen - Directive for iBox tools elements in right corner of ibox with full screen option
 */
function iboxToolsFullScreen($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools_full_screen.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.children('.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
            // Function for close ibox
            $scope.closebox = function () {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            };
            // Function for full screen
            $scope.fullscreen = function () {
                var ibox = $element.closest('div.ibox');
                var button = $element.find('i.fa-expand');
                $('body').toggleClass('fullscreen-ibox-mode');
                button.toggleClass('fa-expand').toggleClass('fa-compress');
                ibox.toggleClass('fullscreen');
                setTimeout(function () {
                    $(window).trigger('resize');
                }, 100);
            }
        }
    };
}

/**
 * minimalizaSidebar - Directive for minimalize sidebar
 */
function minimalizaSidebar($timeout) {
    return {
        restrict: 'A',
        template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
        controller: function ($scope, $element) {
            $scope.minimalize = function () {
                $("body").toggleClass("mini-navbar");
                if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                    // Hide menu in order to smoothly turn on when maximize menu
                    $('#side-menu').hide();
                    // For smoothly turn on menu
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 200);
                } else if ($('body').hasClass('fixed-sidebar')) {
                    $('#side-menu').hide();
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 100);
                } else {
                    // Remove all inline style from jquery fadeIn function to reset menu state
                    $('#side-menu').removeAttr('style');
                }
            }
        }
    };
}

function loopumber() {
    return function (input, total) {
        total = parseInt(total);

        for (var i = 0; i < total; i++) {
            input.push(i);
        }

        return input;
    };

}

function select2() {
    return {
        restrict: 'C',
        link: function (scope, element, attrs) {
            if ($('.select').length > 0) {
                $('.select').select2({
                    minimumResultsForSearch: -1,
                    width: '100%'
                });
            }
        }
    }

}


var table;

function untukDataTable($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {


            var option = {
                responsive: true,
                "autoWidth": false

            };
            if (attrs.order > -1) {
                option.order = [[attrs.order, "desc"]];
            } else {
                option.order = [];
            }
            console.log(option)

            console.log('  document ready function, add search by column feature ');
            table = element.DataTable(option);


        }

    };


}


function untukDataTable_scroll($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {


            var option = {
                "scrollX": true,
                "autoWidth": false,
                fixedColumns:   {
                    leftColumns: 1
                }

            };
            if (attrs.order > -1) {
                option.order = [[attrs.order, "desc"]];
            } else {
                option.order = [];
            }
            console.log(option)

            console.log('  document ready function, add search by column feature ');
            table = element.DataTable(option);


        }

    };


}


/**
 *
 * Pass all functions into module
 */
angular
    .module('inspinia')
    .directive('pageTitle', pageTitle)
    .directive('sideNavigation', sideNavigation)
    .directive('iboxTools', iboxTools)
    .directive('minimalizaSidebar', minimalizaSidebar)
    .directive('iboxToolsFullScreen', iboxToolsFullScreen)
    .directive('select', select2)

    .directive('myDataTable', untukDataTable)
    .directive('myDataTableScroll', untukDataTable_scroll)
    .directive('rage', loopumber);


angular
    .module('inspinia')
    .directive('datepicker', function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, el, attr, ngModel) {
                jQuery(el).datepicker({format: 'dd MM yyyy'})
                    .on('changeDate', function (ev) {
                        console.log(ev);
                        console.log($(ev.target).val());

                        ngModel.$viewValue = ev.date;
                        // ngModel.$commitViewValue();
                        ngModel.$setViewValue(ev.date);

                    });
            }
        };
    })
    .directive('datepickerMonthYear', function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, el, attr, ngModel) {
                jQuery(el).datepicker({
                    format: 'MM yyyy',
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    }
                );

                    // .on('changeDate', function (ev) {
                    //
                    //     ngModel.$viewValue = ev.date;
                    //     ngModel.$setViewValue(ev.date);
                    //
                    // });
            }
        };
    });
