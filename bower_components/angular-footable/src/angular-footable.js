'use strict';


angular
    .module('ui.footable', [])
    .directive('footable', function ($timeout) {
        var events = {
            beforeFiltering: 'footable_filtering'
        };
        var extractSpecOpts = function (opts, attrs) {
            var extracted = {},
                k;
            for (k in opts) {
                if (k !== 'filter' && (!angular.isUndefined(events[k]))) {
                    if (!angular.isFunction(scope.$eval(attrs[k]))) {
                        extracted[k] = attrs[k];
                    }
                }
            }
            return extracted;
        };

        var bindEventHandler = function (tableObj, scope, attrs) {
            var k;
            for (k in attrs) {
                if (k !== 'filter' && (!angular.isUndefined(events[k]))) {
                    var targetEventName = events[k];
                    if (angular.isFunction(scope.$eval(attrs[k]))) {
                        tableObj.bind(targetEventName, scope.$eval(attrs[k]));
                    }
                }
            }
        };

        var tableObj, tableOpts;
        return {
            restrict: 'C',
            link: function (scope, element, attrs) {

                scope.$watch('footable_perpage', function (val) {
                    console.log(val)
                    if(FooTable.get('.footable') !== undefined)
                    {
                        FooTable.get('.footable').pageSize(val);
                    }

                });



                scope.$watch('loadData', function (val) {

                    if (val === true) {
                        scope.footable_perpage_val = [10, 25, 100];
                        $timeout(function () {

                            element.footable();

                        }, 500);
                    }

                });


            }
        };
    });
